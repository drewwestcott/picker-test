//
//  SettingsIC.swift
//  Picker-Test WatchKit Extension
//
//  Created by Drew Westcott on 20/11/2019.
//  Copyright © 2019 Drew Westcott. All rights reserved.
//

import WatchKit

class SettingsIC: WKInterfaceController {

    // MARK: Outlets
    @IBOutlet weak var targetPickerOutlet: WKInterfacePicker!

    // MARK: Initialisers
    let localData = UserDefaults.standard
    var target = 10000
    var newValue : Int = 10000
    var pickerItems = [WKPickerItem]()
    weak var extensionDelegate = ExtensionDelegate()

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        refreshPickerItems()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        targetPickerOutlet.setItems(pickerItems)

        if let storedTarget = localData.object(forKey: "target") as? Double {
            target = Int(storedTarget)
        }
        let watchTarget = round(Double(target)/100)
        targetPickerOutlet.setSelectedItemIndex(Int(watchTarget)-1)
        targetPickerOutlet.focus()
    }
    

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func targetPickerAction(_ value: Int) {
        newValue = value
    }
    
    func refreshPickerItems(){
        pickerItems.removeAll(keepingCapacity: true)
        for target in 1...200 {
            let pickerItem = WKPickerItem()
            pickerItem.title = "\(target*100)"
            pickerItems.append(pickerItem)
        }
    }
    
    @IBAction func setTapped() {
        
        // TODO: - Need to refresh UI and Complication
        extensionDelegate?.updateTarget(newTarget: (newValue + 1)*100)
        pickerItems.removeAll()
        self.pop()
    }
    
    deinit {
        print("Settings deinit")
    }
    
}
