//
//  InterfaceController.swift
//  Picker-Test WatchKit Extension
//
//  Created by Drew Westcott on 20/11/2019.
//  Copyright © 2019 Drew Westcott. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    @IBOutlet weak var targetLabel: WKInterfaceLabel!
    @IBOutlet var percentageLabel: WKInterfaceLabel!
    @IBOutlet var progressDisplay: WKInterfaceGroup!

    let localData = UserDefaults.standard
    var target = 10000

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if let storedTarget = localData.object(forKey: "target") as? Double {
            target = Int(storedTarget)
            targetLabel.setText("Target: \(target)")
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func showSettings() {
        
        pushController(withName: "Settings", context: nil)
        
    }

}
